import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { MySongService } from './my-song.service';
import { Song } from './song';

@Component({
  selector: 'app-my-song',
  templateUrl: './my-song.component.html',
  styleUrls: ['./my-song.component.css']
})
export class MySongComponent implements OnInit {

  public chain: string;
  public foundSong: Song;
  public songGotten: boolean;

  constructor(private songService: MySongService, private activateRoute: ActivatedRoute, private s: DomSanitizer) {
    this.chain = '';
    this.foundSong = new Song();
    this.songGotten = false;
  }

  ngOnInit(): void {
    this.getParams();
    this.findSong();
  }

  public async findSong(): Promise<void>{
    try {
      this.foundSong = await this.songService.getSongByChain(this.chain);
      this.songGotten = true;
    } catch (error) {
      alert('Error al obtener la canción');
    }
  }

  public getParams(): void {
    this.chain = this.activateRoute.snapshot.params.id;
  }

}
