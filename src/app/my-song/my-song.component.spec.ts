import { fn } from '@angular/compiler/src/output/output_ast';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { MySongComponent } from './my-song.component';
import { MySongService } from './my-song.service';
import { Song } from './song';

describe('MySongComponent', () => {
  let component: MySongComponent;
  let fixture: ComponentFixture<MySongComponent>;
  let songServiceMock: any;
  let fakeSong: Song;
  let activatedRouterMock: any;

  beforeEach(async () => {
    fakeSong = new Song();
    fakeSong.chain = '01001';
    fakeSong.name = 'Soy peor';
    fakeSong.url = 'google.com';
    songServiceMock = { getSongByChain: async () => {  Promise.resolve(fakeSong); } };
    activatedRouterMock = { snapshot: {params: {id: '1111'}}};

    await TestBed.configureTestingModule({
      declarations: [ MySongComponent ],
      providers: [ { provide: MySongService, useValue: songServiceMock },
      { provide: ActivatedRoute, useValue: activatedRouterMock}],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MySongComponent);
    component = fixture.componentInstance;
    component.chain = fakeSong.chain;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.songGotten).toBeFalsy();
    expect(component.chain.length).toBeGreaterThan(0);
  });

  it('should obtain a song', async () => {
    const spy = spyOn(songServiceMock , 'getSongByChain').and.returnValue(Promise.resolve(fakeSong));
    component.chain = fakeSong.chain;

    await component.findSong();

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(fakeSong.chain);
    expect(component.foundSong).toEqual(fakeSong);

  });
});
