import { fakeAsync, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { MySongService } from './my-song.service';
import { Song } from './song';

describe('MySongService', () => {
  let service: MySongService;
  let fakeSong: Song;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(MySongService);
    fakeSong = new Song();
  });

  beforeEach(() => {
    fakeSong.chain = '01001';
    fakeSong.name = '';
    fakeSong.url = '';
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Validate getQuestionByChain',  async () => {
    const spy = spyOn(service, 'getSongByChain').and.returnValue(Promise.resolve(new Song()));

    await service.getSongByChain(fakeSong.chain);

    expect(spy).toHaveBeenCalledTimes(1);

    expect(spy).toHaveBeenCalledWith(fakeSong.chain);

  });
});
