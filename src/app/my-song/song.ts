export class Song {

    private xName: string;
    private xUrl: string;
    private xChain: string;
    private xId: string;

    constructor() {
        this.xName = '';
        this.xUrl = '';
        this.xChain = '';
        this.xId = '';
    }

    get name(): string { return this.xName; }

    set name(value: string) { this.xName = value; }

    get url(): string { return this.xUrl; }

    set url(value: string) { this.xUrl = value; }

    get chain(): string { return this.xChain; }

    set chain(value: string) { this.xChain = value; }

    get id(): string { return this.xId; }

    set id(value: string) { this.xId = value; }

}
