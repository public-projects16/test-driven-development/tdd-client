import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Song } from './song';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MySongService {

  constructor(private httpClient: HttpClient) { }

  public getSongByChain(chain: string): Promise<Song> {
    return this.httpClient.get<Song>(environment.API + '/song/' + chain).toPromise();
  }
}
