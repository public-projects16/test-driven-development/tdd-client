import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Question } from './Question';
import { questionLabels } from './question.labels';
import { questions } from './questions.example';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {

  public questions: Question[];
  public minSlideIndex: number;
  public maxSlideNumber: number;
  public currentIndex: number;
  public requestString: string;
  public questionLabels = questionLabels;

  constructor(private router: Router) {
    this.questions = [];
    this.minSlideIndex = 0;
    this.maxSlideNumber = 5;
    this.currentIndex = this.minSlideIndex;
    this.requestString = '00000';
  }

  ngOnInit(): void {
    this.getQuestions();
  }

  public getQuestions(): void {
    for (const question of questions) {
      this.questions.push(new Question(question));
    }
  }

  public sumIndex(): void {
    if (this.canSumIndex()) {
      this.currentIndex++;
    }
  }

  public substractIndex(): void {
    if (this.canSubstractIndex()) {
      this.currentIndex--;
    }
  }

  public canSumIndex(): boolean {
    const nexIndex = this.currentIndex + 1;
    return nexIndex <= this.maxSlideNumber;
  }

  public canSubstractIndex(): boolean {
    const nextIndex = this.currentIndex - 1;
    return nextIndex >= this.minSlideIndex;
  }

  public setAnswer(value: string, position: number): void {
    const firstPart = this.requestString.substr(0, position);
    const lastPart = this.requestString.substr(position + 1);
    this.requestString = firstPart + value + lastPart;
  }

  public goToMySong(): void {
    this.router.navigate(['my-song', {id: this.requestString}]);
  }

}
