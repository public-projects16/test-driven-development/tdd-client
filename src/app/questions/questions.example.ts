import { Question } from './Question';

export const questions: Question[] = [
  {
    description: '¿Estas en una relación actualmente?',
    answer1: 'Sí',
    answer2: 'No',
  },
  {
    description: '¿Extrañas a tu ex?',
    answer1: 'Sí',
    answer2: 'No'
  },
  {
    description: '¿Te gusta el reggaeton viejito?',
    answer1: 'Sí',
    answer2: 'No'
  },
  {
    description: '¿Creíste que habias olvidado a alguien hasta que escuchaste la canción?',
    answer1: 'Sí',
    answer2: 'No'
  },
  {
    description: '¿Te pones felíz en la quincena?',
    answer1: 'Sí',
    answer2: 'No'
  }
];
