import { ComponentDriver } from 'angular-unit-component-driver';
import { QuestionsComponent } from './questions.component';

export class QuestionComponentDriver extends ComponentDriver<QuestionsComponent> {

  get previous(): HTMLElement {
    return this.querySelector('.previous');
  }

  get send(): HTMLElement {
    return this.querySelector('.send');
  }

  get end(): HTMLElement {
    return this.querySelector('.end');
  }

  get next(): HTMLElement {
    return this.querySelector('.next');
  }
}
