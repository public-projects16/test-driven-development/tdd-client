interface IQuestions {
  description: string;
  answer1: string;
  answer2: string;
}

export class Question {

  public description: string;
  public answer1: string;
  public answer2: string;

  constructor(interfacee: IQuestions) {
    this.description = interfacee.description;
    this.answer1 = interfacee.answer1;
    this.answer2 = interfacee.answer2;
  }

}
