import { Location } from '@angular/common';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '../routes';
import { Question } from './Question';
import { QuestionComponentDriver } from './question.driver';
import { questionLabels } from './question.labels';

import { QuestionsComponent } from './questions.component';

describe('QuestionsComponent', () => {
  let component: QuestionsComponent;
  let fixture: ComponentFixture<QuestionsComponent>;
  let location: Location;
  let driver: QuestionComponentDriver;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionsComponent ],
      imports: [RouterTestingModule.withRoutes(routes)]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsComponent);
    driver = new QuestionComponentDriver(fixture, TestBed);
    component = fixture.componentInstance;
    location = TestBed.inject(Location);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct DOM', () => {
    expect(component.questions[0]).toBeInstanceOf(Question);
    expect(driver.previous.innerText).toEqual(questionLabels.previous);
    expect(driver.next.innerText).toEqual(questionLabels.next);
  });

  it('Get questions spy', () => {
    const spy = spyOn(component, 'getQuestions');
    component.getQuestions();
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should have the correct slide number', () => {
    expect(component.minSlideIndex).toBe(0);
    expect(component.maxSlideNumber).toBe(5);
    expect(component.currentIndex).toBe(0);
  });

  it('should sum the index number', () => {
    component.sumIndex();
    expect(component.currentIndex).toBe(1);
    component.substractIndex();
    expect(component.currentIndex).toBe(0);
  });

  it('should validate the correct range +', () => {
    for (let index = component.minSlideIndex; index <= 10; index++) {
      component.sumIndex();
    }
    expect(component.currentIndex).toBe(component.maxSlideNumber);
  });

  it('should validate the correct range -', () => {
    component.currentIndex = component.maxSlideNumber;
    for (let index = 10; index >= component.minSlideIndex; index--) {
      component.substractIndex();
    }
    expect(component.currentIndex).toBe(component.minSlideIndex);
  });

  it('Validate string request', () => {
    expect(component.requestString).toEqual('00000');
    component.setAnswer('1', 1);
    expect(component.requestString).toBe('01000');
    component.setAnswer('1', 2);
    expect(component.requestString).toBe('01100');
  });

  it('Validate navigation', fakeAsync(() => {
    component.goToMySong();
    tick();
    expect(location.path()).toBe('/my-song;id=00000');
  }));


});
