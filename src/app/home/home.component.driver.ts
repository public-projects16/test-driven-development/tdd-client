import { ComponentDriver } from 'angular-unit-component-driver';
import { HomeComponent } from './home.component';

export class HomeComponentDriver extends ComponentDriver<HomeComponent> {

  get title(): HTMLElement {
    return this.querySelector('.title');
  }

  get description(): HTMLElement {
    return this.querySelector('.description');
  }

  get button(): HTMLElement {
    return this.querySelector('.button');
  }

  get badImage(): HTMLElement {
    return this.querySelector('.badImage');
  }
}
