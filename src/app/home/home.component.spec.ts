import { Location } from '@angular/common';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '../routes';

import { HomeComponent } from './home.component';
import { HomeComponentDriver } from './home.component.driver';
import { homeLabels } from './home.labels';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let homeComponentDriver: HomeComponentDriver;

  let location: Location;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [RouterTestingModule.withRoutes(routes)]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    homeComponentDriver = new HomeComponentDriver(fixture, TestBed);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Validate DOM', () => {
    expect(homeComponentDriver.title.innerText).toBe(homeLabels.title);
    expect(homeComponentDriver.description.innerText).toBe(homeLabels.description);
    expect(homeComponentDriver.button.innerText).toBe(homeLabels.btn);
  });

  it('Validate navigation', fakeAsync(() => {
    component.goToQuestions();
    tick();
    expect(location.path()).toBe('/questions');
  }));


});
