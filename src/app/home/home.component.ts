import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { homeLabels } from './home.labels';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public labels = homeLabels;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public goToQuestions(): void {
    this.router.navigate(['questions']);
  }

}
