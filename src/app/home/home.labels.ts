export const homeLabels = {
  title: '¿Qué canción de Bad Bunny soy?',
  description: 'Animate a hacer el test para ver que canción de Bad Bunny es la que más te queda',
  btn: 'Empezar quiz',
  badImage: 'https://images.genius.com/567b764e878fe32329c4a820687dba9b.1000x1000x1.png'
};
